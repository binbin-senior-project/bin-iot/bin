import time
import RPi.GPIO as GPIO

GPIOpin1 = -1
GPIOpin2 = -1
GPIOpin3 = -1
GPIOpin4 = -1


# Initial the input pin
def initialIRSensor(pin1,pin2,pin3,pin4):
  global GPIOpin1 
  global GPIOpin2
  global GPIOpin3
  global GPIOpin4

  GPIOpin1 = pin1
  GPIOpin2 = pin2
  GPIOpin3 = pin3
  GPIOpin4 = pin4
  
  GPIO.setmode(GPIO.BCM)
  GPIO.setup(GPIOpin1,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
  GPIO.setup(GPIOpin2,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
  GPIO.setup(GPIOpin3,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
  GPIO.setup(GPIOpin4,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

  print("Finished Initiation")
  print(GPIOpin1, GPIOpin2, GPIOpin3, GPIOpin4)

def detectSize():
  if(GPIOpin1 != -1 and GPIOpin2 != -1 and GPIOpin3 != -1 and GPIOpin4 != -1):
    ir1 = GPIO.input(GPIOpin1)
    ir2 = GPIO.input(GPIOpin2)
    ir3 = GPIO.input(GPIOpin3)
    ir4 = GPIO.input(GPIOpin4)
    
    if (ir1 == 0 and ir2 == 0 and ir3 == 0 and ir4 == 0):
      print("Big Size")
    elif(ir1 == 0 and ir2 == 0 and ir3 == 0):
      print("Medium Size")
    elif(ir1 == 0 and ir2 == 0):
      print("Small Size")
    elif(ir1 == 0):
      print("very Small Size")
    else:
      print("Error")

  else:
    print("Please Initial Input Ports")

# test module
if __name__ == '__main__':
  
  initialIRSensor(17,27,22)
  while True:
    detectSize()
    time.sleep(0.2)